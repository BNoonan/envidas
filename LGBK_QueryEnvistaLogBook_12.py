


'''PURPOSE'''
'''--------------------------------------------------------------------------'''




'''HISTORY'''
'''--------------------------------------------------------------------------'''


'''RESOURCES'''
'''--------------------------------------------------------------------------'''
# auto scroll bar - http://effbot.org/zone/tkinter-autoscrollbar.htm


'''IMPORT'''
'''--------------------------------------------------------------------------'''
from tkinter import *
import pypyodbc as pydb

import os
import datetime

import json


'''PROGRAM'''
'''--------------------------------------------------------------------------'''




class AutoScrollbar(Scrollbar):
    # a scrollbar that hides itself if it's not needed.
    def set(self, lo, hi):
        if float(lo) <= 0.0 and float(hi) >= 1.0:
            # grid_remove is currently missing from Tkinter!
            self.tk.call("grid", "remove", self)
        else:
            self.grid()
        Scrollbar.set(self, lo, hi)
    def pack(self, **kw):
        raise TclError("cannot use pack with this widget")
    def place(self, **kw):
        raise TclError( "cannot use place with this widget")




class MainWindow():
    def __init__(self,root):


        ''' CONNECT TO DATABASE'''
        '''--------------------------------------------------'''

        conn = pydb.connect(r'Driver={SQL Server};Server=SQL2012Prod03;uid=EnvistaApp;pwd=snIb7v!g32;database=Envista')
        self.cur=conn.cursor()

        self.MaintType=['All','Scheduled - weekly','Scheduled - biweekly','Scheduled - Monthly', 'Scheduled - Quarterly','Scheduled - Annual','Scheduled - Other','Unscheduled - Event']
        self.Category=['All','Other','CO','NOX','SO2','O3','SHARP-PM2.5','SHARP-PM10','DICHOT','TEOM-PM2.5','TEOM-PM10','Communications','Datalogger','Temperature','RH','Wind Direction','Wind Speed','Solar Radiation','Precipitation','Air Pressure','Building']
        self.instType=[]
        self.calibData=[]

        # CREATE SCROLLBAR
        vscrollbar = AutoScrollbar(root)
        vscrollbar.grid(row=0, column=1, sticky=N+S)
        hscrollbar = AutoScrollbar(root, orient=HORIZONTAL)
        hscrollbar.grid(row=1, column=0, sticky=E+W)

        self.canvas = Canvas(root,
                        yscrollcommand=vscrollbar.set,
                        xscrollcommand=hscrollbar.set,width=1200,height=400)
        self.canvas.grid(row=0, column=0, sticky=N+S+E+W)

        vscrollbar.config(command=self.canvas.yview)
        hscrollbar.config(command=self.canvas.xview)

        self.canvas.bind_all("<MouseWheel>", self._on_mousewheel)  


        # MAKE CANVAS EXPANDABLE
        #----------------------------------------------------------------
        root.grid_rowconfigure(0, weight=1)
        root.grid_columnconfigure(0, weight=1)

        
        # DECLARE FRAMES
        #----------------------------------------------------------------
        mainColour='white'
        self.frame = Frame(self.canvas,background=mainColour)
        self.frame.rowconfigure(1, weight=1)
        self.frame.columnconfigure(1, weight=1)

        queryColour='light sea green'
        self.frameQuery=Frame(self.frame,background=queryColour)
        self.frameQuery.grid(row=0, column=0,padx=4, pady=6,sticky='news')

        statusColour='white'
        self.frameStatus=Frame(self.frame,background=statusColour)
        self.frameStatus.grid(row=0,column=1,padx=4,pady=6,sticky='news')


        '''SEARCH SELECTION'''
        # STATION
        stationNames=['All','Ashburton','Geraldine','Kaiapoi','Lyttelton','Riccarton Road','Rangiora','NIWA','St Albans','Timaru Anzac Square','Woolston','Waimate Kennedy','Washdyke']       
        self.selectedStation = StringVar(root)
        self.selectedStation.set(stationNames[0]) # default value
        Label(self.frameQuery, text="Station:",width=20,background=queryColour).grid(row=1, column=0)
        ent2 = OptionMenu(self.frameQuery,self.selectedStation, *stationNames)
        ent2.grid(row=1, column=1,sticky='ew')


        # TECHNICIAN
        res=["B.Noonan","N.Cross","R.Cressy","T.Aberkane","Other","All"]
        self.tech = StringVar(root)
        self.tech.set(res[0]) # default value
        jmi=Label(self.frameQuery, text="Technician:",width=20,background=queryColour).grid(row=2, column=0)
        ent2 = OptionMenu(self.frameQuery,self.tech, *res)
        ent2.grid(row=2, column=1,sticky='ew')

        # START DATE
        self.stTime = Entry(self.frameQuery,width=20)
        self.stTime.insert(END,'6/3/2016')
        Label(self.frameQuery, text='Start Date:',width=20,background=queryColour).grid(row=3, column=0)
        self.stTime.grid(row=3, column=1,sticky='w')

        # END DATE
        self.edTime = Entry(self.frameQuery,width=20)
        Label(self.frameQuery, text='End Date:',width=20,background=queryColour).grid(row=4, column=0)
        self.edTime.grid(row=4, column=1,sticky='w')

        # MAINTENANCE TYPE
        self.type = StringVar(root)
        self.type.set("All") # default value
        Label(self.frameQuery, text="Maintenance Type:",width=20,background=queryColour).grid(row=1, column=2)
        ent7 = OptionMenu(self.frameQuery,self.type, *self.MaintType)
        ent7.grid(row=1, column=3,sticky='ew')

        # CATEGORY
        self.cat = StringVar(root)
        self.cat.set("All") # default value
        Label(self.frameQuery, text='Category:',width=20,background=queryColour).grid(row=2, column=2)
        ent1 = OptionMenu(self.frameQuery,self.cat, *self.Category)
        ent1.grid(row=2, column=3,sticky='ew')

        # SERIAL NUMBER
        self.serialN = Entry(self.frameQuery,width=20)
        Label(self.frameQuery, text='Serial Number:',width=20,background=queryColour).grid(row=3, column=2)
        self.serialN.grid(row=3, column=3,padx=4, pady=6,sticky='w')

        # SEARCH WORD
        self.searchWord = Entry(self.frameQuery,width=20)
        Label(self.frameQuery, text='Find:',width=20,background=queryColour).grid(row=4, column=2)
        self.searchWord.grid(row=4, column=3,padx=4, pady=6,sticky='w')
        self.checkA = IntVar()
        self.checkB = IntVar()
        self.checkC = IntVar()
        self.checkD = IntVar()
        Checkbutton(self.frameQuery,  text="Issue",variable=self.checkA).grid(row=4, column=4,padx=4, pady=6,sticky='w')
        Checkbutton(self.frameQuery,  text="Work" ,variable=self.checkB).grid(row=4, column=5,padx=4, pady=6,sticky='w')
        Checkbutton(self.frameQuery,  text="Spare",variable=self.checkC).grid(row=4, column=6,padx=4, pady=6,sticky='w')
        Checkbutton(self.frameQuery,  text="To Do",variable=self.checkD).grid(row=4, column=7,padx=4, pady=6,sticky='w')

        # SEARCH BUTTON
        showButton = Button(self.frameQuery, text='Search',command=self.searchEntries,width=40)
        showButton.grid(row=10,column=1,columnspan=2)


        '''DISPLAY RESULTS'''
        # Print result of search
        Label(self.frame, text='Result').grid(row=5, column=0,columnspan=8,sticky='ns')
        self.results= Frame(self.frame)
        self.results.grid(row=6, column=0,columnspan=8,padx=4, pady=6,sticky='ns')

        # HEADER
        self.numcolumns=12
        self.columnWidth=[15,8,8,20,8,20,20,15,40,40,40,40]
        self.HeaderText=['     Date  ','Station','Calib Sheet','Maintenance Type','Invalid Data','Technician','Category','Serial Number','Issue','Work Performed','Spare Parts','Future Work']
        for column in range(self.numcolumns):
            label = Label(self.results, text= self.HeaderText[column], 
                                 borderwidth=0,width=self.columnWidth[column])
            label.grid(row=0, column=column, padx=1, pady=1)
 

        '''SEARCH STATUS WINDOW'''
        Label(self.frameStatus, text='Search status').grid(row=0, column=0,sticky='ns')
        self.status = Text(self.frameStatus,height=6,width=150,font=("Purisa", 8))
        self.status.grid(row=1, column=0,padx=4, pady=6,sticky='ns')
        scrollc = Scrollbar(self.frameStatus, command=self.status.yview)
        scrollc.grid(row=1, column=1, padx=4,pady=6,sticky='nsw')
        self.status['yscrollcommand'] = scrollc.set



        ''' UPDATE CANVAS ? '''
        '''----------------------------------------------------------------'''
        self.canvas.create_window(0, 0, anchor=NW, window=self.frame)
        self.frame.update_idletasks()
        self.canvas.config(scrollregion=self.canvas.bbox("all"))


        

    def __del__(self):
        # close database on exit
        self.db.close()


    def _on_mousewheel(self, event): # v6.0.1
        self.canvas.yview_scroll(-1*(event.delta/120), "units")


    def calibrationWindow(self,j):

        window_bgColour='light grey'
        window_fgColour='black'
        label_bgColour='light blue'
        label_fgColour='black'

        window = Toplevel(root)
        window.title('Instrument Calibration/Maintenance Checksheet')
        self.Calibcanvas = Canvas(window,width=1200,height=400)
        self.Calibcanvas.grid(row=0, column=0, sticky='news')
        self.whichframe=Frame(self.Calibcanvas)
        self.whichframe.grid(row=0, column=0, sticky='news')
        self.whichframe.configure(bg=window_bgColour)

        f=str(j)
        g=f.split('**')
        t=g[0]

        print('instrument '+ t)
        print(g)

        with open('Instrument_' +t+ '.json') as data_file:    
            data = json.load(data_file)



       
        # ADD TTTLE TO FRAME
        Label(self.whichframe, text='Instrument Type: '+g[0]+'  Revision: '+g[1]+'  Serial Number: '+g[2], width=20).grid(row=0, column=0,columnspan=4,sticky='news')

        # CREATE SUBFRAMES
        F_actions=Frame(self.whichframe)
        F_actions.grid(row=1,column=0,sticky='ew',padx=10,pady=10)        
        F_actions.configure(bg=window_bgColour)

        F_currentValues=Frame(self.whichframe)
        F_currentValues.grid(row=2,column=0,sticky='ew',padx=10,pady=10)        
        F_currentValues.configure(bg=window_bgColour)

        F_conditions=Frame(self.whichframe)
        F_conditions.grid(row=3,column=0,sticky='ew',padx=10,pady=10)        
        F_conditions.configure(bg=window_bgColour)

        F_calibrations=Frame(self.whichframe)
        F_calibrations.grid(row=4,column=0,sticky='ew',padx=10,pady=10)        
        F_calibrations.configure(bg=window_bgColour)

        F_calEquipment=Frame(self.whichframe)
        F_calEquipment.grid(row=5,column=0,sticky='ew',padx=10,pady=10)        
        F_calEquipment.configure(bg=window_bgColour)



        count5=3

        # POPULATE ACTIONS SECTION
        #-----------------------------------------
        # Calculate number of rows needed to display - if odd number of actions then compensate
        num_actions=len(data['Actions'])
        if (len(data['Actions']) % 2 != 0):
            num_actions+=1
        num_rows=num_actions/2
        colspacer=0 
        row=1

        # display actions 
        for Action in data['Actions']:

            Label(F_actions, text=Action['text'],width=20,relief=GROOVE).grid(row=row,column=0+colspacer,sticky='nes')
            Label(F_actions, text=g[count5],width=20,bg=label_bgColour,fg=label_fgColour,relief=GROOVE).grid(row=row,column=2+colspacer,sticky='nes')

            row+=1
            if row == num_rows:
                row=1
                colspacer=3

            count5+=1                    

        # POPULATE CURRENT VALUES SECTION
        #-----------------------------------------
        colspacer=0 
        row=0
        count6=0

        num_conditions=len(data['Current Values'])
        if (len(data['Current Values']) % 2 != 0):
            num_conditions+=1
        num_rows=num_conditions/2

        for Cond in data['Current Values']:

            Label(F_currentValues, text=Cond['text'],width=20,relief=GROOVE).grid(row=row,column=0+colspacer,sticky='news')
            Label(F_currentValues, text=Cond['lim_A'],width=7,relief=GROOVE).grid(row=row,column=1+colspacer,sticky='news')
            Label(F_currentValues, text=Cond['lim_B'],width=7,relief=GROOVE).grid(row=row,column=2+colspacer,sticky='news')
            Label(F_currentValues, text=g[count5],width=7,bg=label_bgColour,fg=label_fgColour,relief=GROOVE).grid(row=row,column=3+colspacer,sticky='news')
            count5+=1

            row+=1
            if row == num_rows:
                row=0
                colspacer=5



        #INSTRUMENT CONDITIONS
        #-----------------------------------------
        colspacer=0 
        row=0

        num_conditions=len(data['Instrument Conditions'])
        if (len(data['Instrument Conditions']) % 2 != 0):
            num_conditions+=1
        num_rows=num_conditions/2

        for Cond in data['Instrument Conditions']:

            Label(F_conditions, text=Cond['text'],width=20,relief=GROOVE).grid(row=row,column=0+colspacer,sticky='news')
            Label(F_conditions, text=Cond['lim_A'],width=7,relief=GROOVE).grid(row=row,column=1+colspacer,sticky='news')
            Label(F_conditions, text=Cond['lim_B'],width=7,relief=GROOVE).grid(row=row,column=2+colspacer,sticky='news')


            bgCol='white'
            testCond=Cond['valid']
            print('hello')
            print(g[count5])
            #number=float(g[count5])
            lim_l=float(Cond['lim_A'])
            lim_u=float(Cond['lim_B'])

            print(lim_l)

            try:
                float(g[count5])
                print(g[count5])
                number=float(g[count5])
                if testCond == 'between':                
                    if number < lim_l or number > lim_u:
                        bgCol='pink'

                if testCond == 'equal':
                    if number != lim_l:
                        bgCol='pink'


                if testCond =='or': 
                    if (number != lim_l) and (number !=float(Cond['lim_B'])):
                        bgCol='pink'
            except:
                print('nup')


            Label(F_conditions, text=g[count5],width=7,bg=bgCol,fg=label_fgColour,relief=GROOVE).grid(row=row,column=3+colspacer,sticky='news')
            count5+=1

            try:
                float(g[count5])
                print(g[count5])
                number=float(g[count5])
                if testCond == 'between':                
                    if number < lim_l or number > lim_u:
                        bgCol='pink'

                if testCond == 'equal':
                    if number != lim_l:
                        bgCol='pink'


                if testCond =='or': 
                    if (number != lim_l) and (number !=float(Cond['lim_B'])):
                        bgCol='pink'
            except:
                print('nup')



            Label(F_conditions, text=g[count5],width=7,bg=bgCol,fg=label_fgColour,relief=GROOVE).grid(row=row,column=4+colspacer,sticky='news')
            count5+=1

            row+=1
            if row == num_rows:
                row=0
                colspacer=5


        #CALIBRATION INFORMATION
        #-----------------------------------------
        tableCount=0
        for Tables in data['Calibration Information']:

            colNum=1
            rowNum=2

            for x in Tables['Headers']:
                Label(F_calibrations, text=x,width=10,relief=GROOVE).grid(row=1,column=colNum,sticky='news')
                colNum+=1

            for y in Tables['Ys']:
                Label(F_calibrations, text=y,width=15,relief=GROOVE).grid(row=rowNum,column=0,sticky='news')
                rowNum+=1

            for a in range(2,rowNum):
                for b in range(1,colNum):
                    Label(F_calibrations, text=g[count5],width=15,bg=label_bgColour,fg=label_fgColour,relief=GROOVE).grid(row=a,column=b,sticky='news')
                    count5+=1
            
            tableCount+=1


        #CALIBRATION EQUIPMENT
        #-----------------------------------------
        colNum=0
        
        for calEquip in data['Calibration Equipment']:

            for x in calEquip['Headers']:
                Label(F_calEquipment, text=x,width=10,relief=GROOVE).grid(row=0,column=colNum,sticky='news')
                Label(F_calEquipment, text=g[count5],width=10,bg=label_bgColour,fg=label_fgColour,relief=GROOVE).grid(row=1,column=colNum,sticky='news')

                colNum+=1
                count5+=1


    def searchEntries(self):

        '''CLEAR PREVIOUS RESULTS'''
        '''--------------------------------------------'''
        self.results.destroy()
        self.results= Frame(self.frame)
        self.results.grid(row=6, column=0,columnspan=4,padx=4, pady=6,sticky='ns')

        # HEADER
        for column in range(self.numcolumns):
            label = Label(self.results, text= self.HeaderText[column], 
                                 borderwidth=0, width=self.columnWidth[column])
            label.grid(row=0, column=column, sticky="nsew", padx=1, pady=1)   



        '''BUILD SQL STATEMENT'''
        '''--------------------------------------------'''
        # find station code for selected station
        self.cur.execute("""SELECT STA_SerialCode FROM [Envista].[dbo].[TB_STATION] where STA_StationName='"""+self.selectedStation.get()+"""' """)
        self.stnID=self.cur.fetchall()

        killSearch=0
        startTime=self.stTime.get()
        endTime=self.edTime.get() 

        # Test time stamps for acceptability
        #-------------------------------------
        # If neither start or end times are given
        if not self.stTime.get() and not self.edTime.get():
            self.status.insert(END,'ENTER DATES: search cancelled \n')
            killSearch=1

        # Check start date if end date given
        if self.edTime.get() and not self.stTime.get():
            # Print Status to window
            self.status.insert(END,'CHECK DATES: End date but no start date \n') 
            killSearch=1

        # if no end date given then make it now
        if not self.edTime.get() and self.stTime.get():
            t=datetime.datetime.now()
            endTime=str(t.year) +'-'+ str(t.month) +'-'+ str(t.day) +' '+ str(t.hour) +':'+ str(t.minute)


        if killSearch==0:

            conditions=[]
            if len(self.stnID)>0:
                conditions.append("""StationID = '"""+str(self.stnID[0][0])+"""' """)
            else:
                conditions.append(""" """)
            conditions.append("""Technician = '"""+self.tech.get()+"""' """)
            conditions.append("""TendType = '"""+self.type.get()+"""' """)    
            conditions.append(""" Date_Time BETWEEN '"""+startTime+"""' AND '"""+endTime+"""'  """)
            conditions.append(""" (Equipment = 'Soil' OR Equipment= 'TRS') """)

          
            ignore=[0,0,0,0,0]
            if self.selectedStation.get()=='All': # ignore station 
                ignore[0]=1
            if self.tech.get()=='All':  # Ignore technician
                ignore[1]=1
            if self.type.get()=='All':  # Ignore maintenance type
                ignore[2]=1
           
            sql= """ SELECT * FROM [Envista].[dbo].[TB_LOGBOOK] WHERE """

            # add conditions if specified
            cnt=0
            for x in conditions:
                if ignore[cnt]==0:
                    sql=(sql+x)

                    if cnt<len(conditions)-1:
                        sql=(sql+' and ')
                cnt+=1

            # Print Status to window
            self.status.insert(END,'SEARCH: '+sql+' \n') 

            print(sql)
                        

            '''EXECUTE SQL STATEMENT AND STORE DATA IN LIST'''
            '''--------------------------------------------'''
            result=self.cur.execute(sql)
            d=result.fetchall()

            

            print(d)

            whichCheckboxes=[self.checkA.get()*3,self.checkB.get()*4,self.checkC.get()*5,self.checkD.get()*6]

            e=[]
            count2=0
            for i in d:
                
                if i[2]=='Soil':
                    print('Form')

                    r=[]
                    keepRec=1

                    f=str(i[6])
                    g=f.split('**',6)

                    # Retrieve station name to make more readable
                    self.cur.execute("""SELECT STA_StationName FROM [Envista].[dbo].[TB_STATION] where STA_SerialCode='"""+str(i[1])+"""' """)
                    stnName=self.cur.fetchall()
                    print(i)
                    r.extend((i[0],stnName[0][0],"",i[3],i[4],i[5],g[1],g[2],g[3],g[4],g[5],g[6]))
                    self.status.insert(END,'SEARCH RESULTS: '+str(r)+' \n') 


                    ''' FURTHER REFINE SEARCH BY CATEGORY, SERIAL NUMBER AND SEARCH WORD'''
                    '''-----------------------------------------------------------------'''
                    # Eliminate by category
                    found=[0,0,0]
                    if self.cat.get()!='All':
                        if g[1]!=self.cat.get():
                            keepRec=0

                    # Then by serial number -
                    if self.serialN.get() and self.cat.get()!='All':
                        if g[2]!=self.serialN.get():
                            keepRec=0


                    if self.serialN.get() and self.cat.get()=='All':
                        print('need to specify category')
                        keepRec=0

                    # look for search word
                    count=0
                    if self.searchWord.get():

                        for p in whichCheckboxes:
                            if p!=0:
                                if not g[p].find(self.searchWord.get()): 
                                    count=count+1
                                print(count)

                        if count==0:
                            keepRec=0


                    if keepRec==1:
                        e.append(r)



    

                
                    # RETRIEVED DATA
                    for column in range(self.numcolumns):
                        label = Text(self.results,borderwidth=0,background='white',width=self.columnWidth[column],height=5,font=("Purisa", 8))
                        label.insert(END,e[count2][column])
                        label.grid(row=count2+1, column=column, sticky="nsew", padx=1, pady=1)

                    count2+=1

                if i[2]=='TRS':
                    print('Calibration')

                    viewCalib = Button(self.results, text='View',command= lambda i=i: self.calibrationWindow(i[6]),width=20)
                    viewCalib.grid(row=count2,column=2,columnspan=1)




                   



 















root = Tk()
MainWindow(root)
root.mainloop()