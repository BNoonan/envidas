'''PURPOSE'''
'''--------------------------------------------------------------------------'''

'''HISTORY'''
'''--------------------------------------------------------------------------'''

'''RESOURCES'''
'''--------------------------------------------------------------------------'''
# auto scroll bar - http://effbot.org/zone/tkinter-autoscrollbar.htm
# create new entries with button - http://stackoverflow.com/questions/24833655/creating-new-entry-boxes-with-button-tkinter



'''ISSUES'''
'''--------------------------------------------------------------------------'''
# leak calculator

'''TO DO'''
'''--------------------------------------------------------------------------'''
# clean up
# instrument sheets
# add-ons
# query


'''IMPORT MODULES'''
'''--------------------------------------------------------------------------'''
from tkinter import *
import pypyodbc as pydb
import os
import datetime
from datetime import datetime, date
import json


'''PROGRAM'''
'''--------------------------------------------------------------------------'''

class AutoScrollbar(Scrollbar):
    # a scrollbar that hides itself if it's not needed.
    def set(self, lo, hi):
        if float(lo) <= 0.0 and float(hi) >= 1.0:
            # grid_remove is currently missing from Tkinter!
            self.tk.call("grid", "remove", self)
        else:
            self.grid()
        Scrollbar.set(self, lo, hi)
    def pack(self, **kw):
        raise TclError("cannot use pack with this widget")
    def place(self, **kw):
        raise TclError( "cannot use place with this widget")




class MainWindow():

    '''================================================================================================================'''
    ''' GENERAL FUNCTIONS '''
    '''================================================================================================================'''
    def insertTime(self, b_id, form): 
        # PURPOSE: Insert time into specified entry box when a button is pressed. Can select either date time, date or time formats.

        # Get time now
        t = datetime.now()

        # Seperate time components
        month=str(t.month)
        day=str(t.day)
        hour=str(t.hour)
        minute=str(t.minute)

        # Prettyfy
        if t.month < 10:
            month='0' + str(t.month)

        if t.day < 10:
            day='0'+ str(t.day)

        if t.hour < 10:
            hour='0' + str(t.hour)

        if t.minute < 10:
            minute='0' + str(t.minute)

        # Form into desired shape
        if form=='dt':
            time=str(t.year) +'-'+ month +'-'+ day +' '+ hour +':'+ minute
        if form=='dt/':
            time=str(t.year) +'/'+ month +'/'+ day +' '+ hour +':'+ minute
        if form=='d':
            time=str(t.year) +'-'+ month +'-'+ day
        if form=='t':
            time= hour +':'+ minute

        # Enter into specified entry box
        b_id.delete(0,END)
        b_id.insert(END,time)



    '''================================================================================================================'''
    ''' MAIN WINDOW '''
    '''================================================================================================================'''
    def __init__(self,root):

        # DEFINE FUNCTION VARIABLES
        #==========================================================================
        self.n=2
        self.p=0
        self.t=0
        self.all_entries = []
        self.calibrations ={}
        self.instType={}
        self.instSerial={}
        self.revision={}
        self.endTime={}
        self.startTime={}
        self.calFrame={}
        self.startTimeButton=[]
        self.repeatAction=[]
        self.calType2={}
        self.frame_form_index={}
        self.MaintType=[]
        self.Category=[]
        self.Technician=[]


        # COLOURS
        #==========================================================================
        self.formBackground=['snow3','snow3']
        saveAddBackground='wheat4'
        mainBackground='azure'


        # EXTRACT CONFIGURATION INFORMATION
        #==========================================================================
        for fileName in os.listdir(os.getcwd()):
            if fileName.startswith('Configuration'):
                configFile=fileName

                with open(fileName) as config_file:    
                    config = json.load(config_file)

                for a in config['Maintenance Types']:
                    self.MaintType.append(a)

                for b in config['Service Category']:
                    self.Category.append(b)

                for c in config['Technicians']:
                    self.Technician.append(c)


        # CONNECT TO DATABASE
        #==========================================================================
        try:
            self.db = pydb.connect(r'Driver={SQL Server};Server=AQS14;uid=sa;pwd=EnvitechLTD1;database=EnvidasConfig')
        except:
            print('Error connecting to database')


        # CREATE CANVAS
        #==========================================================================
        # SCROLLBAR
        vscrollbar = AutoScrollbar(root)
        vscrollbar.grid(row=0, column=1, sticky='ns')
        hscrollbar = AutoScrollbar(root, orient=HORIZONTAL)
        hscrollbar.grid(row=1, column=0, sticky='EW')

        # CANVAS
        self.canvas = Canvas(root,
                        yscrollcommand=vscrollbar.set,
                        xscrollcommand=hscrollbar.set,width=695,height=1200,background=mainBackground)
        self.canvas.grid(row=0, column=0, sticky='news')

        vscrollbar.config(command=self.canvas.yview)
        hscrollbar.config(command=self.canvas.xview)

        # SCROLL WITH MOUSE WHEEL
        self.canvas.bind_all("<MouseWheel>", self._on_mousewheel) 

        # MAKE CANVAS EXPANDABLE
        root.grid_rowconfigure(0, weight=1)
        root.grid_columnconfigure(0, weight=1)

        
        # CREATE FORM
        #==========================================================================
        self.frame = Frame(self.canvas,background=mainBackground)
        self.frame.rowconfigure(1, weight=1)
        self.frame.columnconfigure(1, weight=1)


        # SAVE/ADD/EXIT SECTION
        #--------------------------------------------------------------------------
        # CREATE FRAME
        self.frame_saveAdd=Frame(self.frame, background= saveAddBackground)
        self.frame_saveAdd.grid(row=0, column=0,padx=4,pady=6,sticky='news')

        # ADD NEW FORM BUTTON
        Button(self.frame_saveAdd, text='Add another',width=25,command=self.addForm, background= saveAddBackground).grid(row=0,column=0,sticky='ew')

        # SAVE TO DATABASE
        Button(self.frame_saveAdd, text='Submit to database',width=25,command=self.saveEntries, background= saveAddBackground).grid(row=0,column=1,sticky='ew')

        # CLOSE WINDOW AND DATABASE
        Button(self.frame_saveAdd, text='Exit',width=25, command=lambda:(self.db.close(),root.destroy()),background= saveAddBackground).grid(row=0,column=2,sticky='ew')


        # CREATE FIRST ENTRY
        #--------------------------------------------------------------------------
        self.addForm()


        # UPDATE CANVAS ?
        #--------------------------------------------------------------------------
        self.canvas.create_window(0, 0, anchor=NW, window=self.frame)
        self.frame.update_idletasks()
        self.canvas.config(scrollregion=self.canvas.bbox("all"))


    def __del__(self):
        # close database on exit
        self.db.close()


    def _on_mousewheel(self, event):
        # allows mouse wheel to move scrollbars
        self.canvas.yview_scroll(-1*(event.delta/120), "units")


    def addForm(self):
        i=self.t

        # DATABASE STUFF
        #==========================================================================
        cursor = self.db.cursor()

        # CREATE FRAMES
        #==========================================================================
        # COLOUR EACH FRAME ALTERNATE COLOURS
        if self.p==0:
            backgroundClr=self.formBackground[0]
            self.p=1
        else:
            backgroundClr=self.formBackground[1]
            self.p=0
            
        # CREATE MAIN FORM
        self.frame_form=Frame(self.frame, background=backgroundClr)
        self.frame_form.grid(row=self.n, column=0,padx=4,pady=6,sticky='news')
        self.frame_form_index[i]=self.frame_form

        # CREATE TOP ENTRY FORM
        self.frame_top=Frame(self.frame_form, background=backgroundClr)
        self.frame_top.grid(row=0, column=0,padx=4,pady=6,sticky='news')

        # CREATE BOTTOM FORM
        self.frame_bottom=Frame(self.frame_form, background=backgroundClr)
        self.frame_bottom.grid(row=1, column=0,padx=4,pady=6,sticky='news')

        # FETCH STATION ID
        cursor.execute("SELECT DISTINCT ID FROM [EnvidasConfig].[dbo].[Station]")
        res = cursor.fetchall() 
        self.stnID = StringVar(root)
        self.stnID.set(res[0])

        # CATEGORY         
        Label(self.frame_top, text='Category:',width=25, background=backgroundClr).grid(row=1, column=0,sticky='ew')
        self.cat = StringVar(root)
        self.cat.set("Ignore") # default value
        OptionMenu(self.frame_top,self.cat, *self.Category).grid(row=2, column=0,sticky='ew')

        # SERIAL NUMBER
        Label(self.frame_top, text='Serial Number:',width=25, background=backgroundClr).grid(row=1, column=1,sticky='ew')
        self.serialN = Entry(self.frame_top)
        self.serialN.grid(row=2, column=1,padx=4, pady=6,sticky='ew')

        # TECHNICIAN
        Label(self.frame_top, text="Technician:", width=25, background=backgroundClr).grid(row=5, column=0,sticky='w')        
        self.tech = StringVar(root)
        self.tech.set(self.Technician[0]) # default value
        OptionMenu(self.frame_top,self.tech, *self.Technician).grid(row=6, column=0,sticky='ew')

        # MAINTENANCE TYPE
        Label(self.frame_top, text="Maintenance Type:",width=25, background=backgroundClr).grid(row=3, column=0,sticky='w')       
        self.type = StringVar(root)
        self.type.set("Scheduled - Monthly") # default value
        OptionMenu(self.frame_top,self.type, *self.MaintType).grid(row=4, column=0,sticky='ew')

        # START TIME
        self.stTime = Entry(self.frame_top)
        self.stTime.insert(END,'')
        self.stTime.grid(row=2, column=2,padx=4, pady=6,sticky='ew')
        self.startTime[i]=self.stTime
        self.stTimeButton = Button(self.frame_top, text='Start',width=25,command = lambda i=i: self.insertTime(self.startTime[i],'dt'), background=backgroundClr)
        self.stTimeButton.grid(row=1,column=2)
     
        # SELECT END TIME
        self.edTime = Entry(self.frame_top)
        self.edTime.insert(END,'')
        self.edTime.grid(row=4, column=2,padx=4, pady=6,sticky='ew')
        self.endTime[i]=self.edTime
        self.edTimeButton = Button(self.frame_top, text='End',width=25,command = lambda i=i: self.insertTime(self.endTime[i],'dt'), background=backgroundClr)
        self.edTimeButton.grid(row=3,column=2)

        # VALIDATE/INVALIDATE DATA        
        Label(self.frame_top, text='Invalidate Data:', width=25,background=backgroundClr).grid(row=5, column=2)
        self.dataV = StringVar(root)
        self.dataV.set("Yes") # default value
        OptionMenu(self.frame_top,self.dataV, 'Yes','No').grid(row=6, column=2,sticky='ew')

        # ADD CALIBRATION
        # find instrument files
        instrumentTypes=[]
        for file in os.listdir(os.getcwd()):
            if file.startswith("Instrument_"):
                temp=file.replace('Instrument_','')
                temp2=temp.replace('.json','')
                instrumentTypes.append(temp2)

        self.calType = StringVar(root)
        self.calType.set("None") # default value
        OptionMenu(self.frame_top,self.calType,*instrumentTypes).grid(row=6, column=4,sticky='ew')
        self.calType2[i]=self.calType
        self.calib = Button(self.frame_top, text='Maint/Calib Sheet',command= lambda j=i: self.addCalibration(self.frame_form_index[j],self.calType2[j],j,self.serialN.get()))
        self.calib.grid(row=5,column=4)

        # ISSUE/REASON FOR VISIT
        Label(self.frame_bottom, text='Issue', background=backgroundClr).grid(row=0, column=0,columnspan=4,sticky='ns')
        self.issue = Text(self.frame_bottom,height=6,width=70)
        self.issue.grid(row=1, column=0,columnspan=4,padx=4, pady=6,sticky='ns')
        scrollb = Scrollbar(self.frame_bottom, command=self.issue.yview)
        scrollb.grid(row=1, column=4, padx=4,pady=6,sticky='nsw')
        self.issue['yscrollcommand'] = scrollb.set

        # WORK PERFORMED
        Label(self.frame_bottom, text='Work performed', background=backgroundClr).grid(row=2, column=0,columnspan=4,sticky='ns')
        self.desc = Text(self.frame_bottom,height=6,width=70)
        self.desc.grid(row=3, column=0,columnspan=4,padx=4, pady=6,sticky='ns')
        scrollb = Scrollbar(self.frame_bottom, command=self.desc.yview)
        scrollb.grid(row=3, column=4, padx=4,pady=6,sticky='nsw')
        self.desc['yscrollcommand'] = scrollb.set

        # SPARE PARTS
        Label(self.frame_bottom, text='Parts Used', background=backgroundClr).grid(row=4, column=0,columnspan=4)
        self.spare = Text(self.frame_bottom,height=3,width=70)
        self.spare.grid(row=5, column=0,columnspan=4,padx=4, pady=6)

        # FUTURE WORK
        Label(self.frame_bottom, text='Future work', background=backgroundClr).grid(row=6, column=0,columnspan=4)
        self.future = Text(self.frame_bottom,height=3,width=70)
        self.future.grid(row=7, column=0,columnspan=4,padx=4, pady=6)

        # INCREMENT COUNTERS
        self.n=self.n+1
        self.t=self.t+1

        # PASS INFORMATION
        self.all_entries.append( (self.stnID,self.tech,self.type,self.stTime,self.edTime,self.dataV,self.cat,self.serialN,self.issue,self.desc,self.spare,self.future) )

        # ?
        self.canvas.create_window(0, 0, anchor=NW, window=self.frame)
        self.frame.update_idletasks()
        self.canvas.config(scrollregion=self.canvas.bbox("all"))




    '''================================================================================================================'''
    ''' ADD A CALIBRATION OR MAINTENANCE SHEET TO DETAIL WORK PERFORMED'''
    '''================================================================================================================'''
    ''' ---------------------------------------------------------------------------------------------------------------'''
    ''' ADD CALIBRATION FORM '''
    ''' ---------------------------------------------------------------------------------------------------------------'''
    def addCalibration(self,whichframe,whichInstrument2,count,serialNum):
        # PURPOSE - 

        #----------------------------------------------------------------------------
        # GENERAL FUNCTIONS AND VARIABLES   
        #----------------------------------------------------------------------------         
        self.calibEntries={}
        lim_lower=[]
        lim_upper=[]
        lim_test=[]

        currentValues_lowerLim=[]
        currentValues_upperLim=[]
        currentValues_testLimits=[]

        cursor = self.db.cursor()

        def whichEntry(i):
            # PURPOSE - 
            return(i)


        #----------------------------------------------------------------------------
        # CREATE CALIBRATION/MAINTENANCE FRAME AND POPULATE 
        #----------------------------------------------------------------------------
        # CREATE CALIBRATION/MAINTENANCE FRAME
        whichframe = Frame(self.frame_form_index[count])
        whichframe.grid(row=0, column=1, rowspan=2, padx=4,pady=6,sticky='news') 
        self.calFrame[count]=whichframe

        # IF INSTRUMENT SPECIFIED - POPULATE FRAME
        whichInstrument=whichInstrument2.get()
        if whichInstrument!='None':  

            self.canvas.config(width=1700) # adjust window width if calibration added

            # OPEN FILE THAT CONTAINS FORMAT
            with open('Instrument_'+whichInstrument+'.json') as data_file:    
                data = json.load(data_file)

                for details in data['Form Details']:
                    formName=details['Name']
                    pollutantType=details['Pollutant']
                    formRevision=details['Revision']

            # ADD TTTLE TO FRAME
            Label(whichframe, text='Instrument Type: '+formName+'  Serial Number: '+serialNum, width=20).grid(row=0, column=0,columnspan=4,sticky='news')

            # CREATE SUBFRAMES
            Frame_actions=Frame(whichframe)
            Frame_actions.grid(row=1,column=0,sticky='ew',padx=10,pady=10)

            F_currentValues=Frame(whichframe)
            F_currentValues.grid(row=2,column=0,sticky='ew',padx=10,pady=10)

            Frame_conditions=Frame(whichframe)
            Frame_conditions.grid(row=3,column=0,sticky='ew',padx=10,pady=10)

            Frame_calibrations=Frame(whichframe)
            Frame_calibrations.grid(row=4,column=0,sticky='ew',padx=10,pady=10)

            Frame_calEquipment=Frame(whichframe)
            Frame_calEquipment.grid(row=5,column=0,sticky='ew',padx=10,pady=10)

            Frame_addON=Frame(whichframe)
            Frame_addON.grid(row=6,column=0,sticky='ew',padx=10,pady=10)

            count5=0

            


            # POPULATE ACTIONS SECTION
            #-----------------------------------------
            # LOAD OLD ACTIONS AND CHECK DATE TO SEE IF ACTION REQUIRED
            cursor.execute("SELECT * FROM [EnvidasConfig].[dbo].[TB_LOGBOOK] WHERE Equipment='TRS'")
            results = cursor.fetchall()
            print(results)

            e=[]
            for i in results:
                f=str(i[6])
                g=f.split('**',3)
                print(g)
                if (g[0]==whichInstrument) and g[1]==formRevision and (g[2]==serialNum):
                    print(g[0])
                    print(g[1])
                    print(g[2])

                    e.append(f)

            #g=f.split('**') TODO
            lastEntryFound=0
            if len(e)>0:
                lastEntry=e[-1]
                r=lastEntry.split('**',lastEntry.count('**'))
                lastEntryFound=1


            # Calculate number of rows needed to display - if odd number of actions then compensate
            num_actions=len(data['Actions'])
            if (len(data['Actions']) % 2 != 0):
                num_actions+=1
            num_rows=num_actions/2
            colspacer=0 
            row=1

            # display actions with 'now' time button and past times
            nowButton={}
            count7=0
            startDates=3 # index in entry where dates start
            for Action in data['Actions']:

                action_Label = Label(Frame_actions, text=Action['text'],width=20)
                action_Label.grid(row=row,column=0+colspacer,sticky='nes')

                actionText=''
                fgColour='black'
                bkColour='white'            

                # If last entry found and field has date
                if (lastEntryFound==1) and (r[count7+startDates]!=''):
                    # check time between and highlight if due
                    date_object = datetime.strptime(r[count7+startDates], '%Y-%m-%d')
                    duration=str(datetime.now()-date_object)
                    duration2=duration.split(',',2)
                    actionText=r[count7+startDates]
                    if len(duration2)>1:
                        days=duration2[0].split(' days',1)
                        if (int(Action['Due period']) < int(days[0])):
                            fgColour='black'
                            bkColour='pink'

                # If no date and due period non zero
                if (Action['Due period']!='0') and (actionText==''):
                        fgColour='black'
                        bkColour='pink'

                action_Entry = Entry(Frame_actions ,width=20)    
                action_Entry.insert(END,actionText)
                action_Entry.grid(row=row,column=2+colspacer,sticky='news')
                action_Entry.configure(fg=fgColour,background=bkColour)
                
                nowButton[count7]=action_Entry
                action_Button = Button(Frame_actions, text='Now',width=5,command = lambda i=count7: self.insertTime(nowButton[i],'d'))
                action_Button.grid(row=row,column=1+colspacer,sticky='news')

                count7+=1
                row+=1
                if row == num_rows:
                    row=1
                    colspacer=3

                self.calibEntries[count5]=action_Entry
                count5+=1

            # POPULATE CURRENT VALUES SECTION
            colspacer=0 
            row=0
            count6=0

            num_conditions=len(data['Current Values'])
            if (len(data['Current Values']) % 2 != 0):
                num_conditions+=1
            num_rows=num_conditions/2

            for Cond in data['Current Values']:

                Label(F_currentValues, text=Cond['text'],width=20).grid(row=row,column=0+colspacer,sticky='news')

                Label(F_currentValues, text=Cond['lim_A'],width=7).grid(row=row,column=1+colspacer,sticky='news')
                currentValues_lowerLim.append(Cond['lim_A'])

                Label(F_currentValues, text=Cond['lim_B'],width=7).grid(row=row,column=2+colspacer,sticky='news')
                currentValues_upperLim.append(Cond['lim_B'])

                currentValues_testLimits.append(Cond['valid'])

                E_currentValues = Entry(F_currentValues ,width=7,background='light blue',validate="focusout", validatecommand = lambda f= count5: self.validEntry(whichEntry(f),count,currentValues_lowerLim[f],currentValues_upperLim[f],currentValues_testLimits[f] ))
                E_currentValues.grid(row=row,column=3+colspacer,sticky='news')
                self.calibEntries[count5]=E_currentValues
                count5+=1

                row+=1
                if row == num_rows:
                    row=0
                    colspacer=5



            '''INSTRUMENT CONDITIONS'''
            colspacer=0 
            row=0
            count6=0

            num_conditions=len(data['Instrument Conditions'])
            if (len(data['Instrument Conditions']) % 2 != 0):
                num_conditions+=1
            num_rows=num_conditions/2

            for Cond in data['Instrument Conditions']:

                cond_Label = Label(Frame_conditions, text=Cond['text'],width=20)
                cond_Label.grid(row=row,column=0+colspacer,sticky='news')

                cond_LIMA_Label = Label(Frame_conditions, text=Cond['lim_A'],width=7)
                cond_LIMA_Label.grid(row=row,column=1+colspacer,sticky='news')
                lim_lower.append(Cond['lim_A'])

                cond_LIMB_Label = Label(Frame_conditions, text=Cond['lim_B'],width=7)
                cond_LIMB_Label.grid(row=row,column=2+colspacer,sticky='news')
                lim_upper.append(Cond['lim_B'])

                lim_test.append(Cond['valid'])


                cond_Entry = Entry(Frame_conditions ,width=7,background='light blue',validate="focusout", validatecommand = lambda f= count5, h=count6: self.validEntry(whichEntry(f),count,lim_lower[h],lim_upper[h],lim_test[h] ))
                cond_Entry.grid(row=row,column=3+colspacer,sticky='news')
                self.calibEntries[count5]=cond_Entry
                count5+=1

                cond_Entry = Entry(Frame_conditions ,width=7,background='light blue',validate="focusout", validatecommand = lambda f= count5, h=count6: self.validEntry(whichEntry(f),count,lim_lower[h],lim_upper[h],lim_test[h] ))
                cond_Entry.grid(row=row,column=4+colspacer,sticky='news')
                self.calibEntries[count5]=cond_Entry
                count5+=1
                count6+=1

                row+=1
                if row == num_rows:
                    row=0
                    colspacer=5


            ''' CALIBRATION INFORMATION'''
            tableCount=0
            for Tables in data['Calibration Information']:

                colNum=1
                rowNum=2

                for x in Tables['Headers']:
                    calibHeader_Label = Label(Frame_calibrations, text=x,width=10)
                    calibHeader_Label.grid(row=1,column=colNum,sticky='news')
                    colNum+=1

                for y in Tables['Ys']:
                    calibYs_Label = Label(Frame_calibrations, text=y,width=15)
                    calibYs_Label.grid(row=rowNum,column=0,sticky='news')
                    rowNum+=1

                for a in range(2,rowNum):
                    for b in range(1,colNum):
                        calib_Entry = Entry(Frame_calibrations ,width=10)
                        calib_Entry.grid(row=a,column=b,sticky='news')
                        self.calibEntries[count5]=calib_Entry
                        count5+=1
                
                tableCount+=1


            ''' CALIBRATION EQUIPMENT'''
            with open("Equipment.json") as data_file:    
                equipData = json.load(data_file)

            colNum=0
            
            for calEquip in data['Calibration Equipment']:

                for x in calEquip['Headers']:
                    List=[]
                    List.append('N/A')
                    calibEquipHeader_Label = Label(Frame_calEquipment, text=x,width=10)
                    calibEquipHeader_Label.grid(row=0,column=colNum,sticky='news')
                    if x=='Gas Bottle':
                        for t in equipData[x]:
                            for e in t[pollutantType]:
                                List.append(e['Bottle Number'])
                    else:
                        for t in equipData[x]:
                            List.append(t['Name'])

                    firstChoice=StringVar(root)
                    firstChoice.set(List[0])
                    select = OptionMenu(Frame_calEquipment,firstChoice,*List)
                    select.grid(row=1, column=colNum,sticky='ew')

                    colNum+=1
                    self.calibEntries[count5]=firstChoice
                    count5+=1


            for addOn in data['Add-ons']:
                if addOn['name'] == 'Gas Calibration':
                    self.addCal(Frame_addON,count)
                if addOn['name'] == 'TEOM leak check':
                    self.leakCheck(Frame_addON,count,'TEOM')
                if addOn['name'] == 'Dichot leak check':
                    self.leakCheck(Frame_addON,count,'Dichot')


            '''STORE MAINTENANCE/CALIBRATION INFORMATION'''
            self.calibrations[count]=self.calibEntries
            self.instType[count]=whichInstrument
            self.instSerial[count]=serialNum
            self.revision[count]=formRevision


    ''' ---------------------------------------------------------------------------------------------------------------'''
    ''' ADD LEAK CHECK CALCULATOR '''
    ''' ---------------------------------------------------------------------------------------------------------------'''
    def leakCheck(self,frame,count,PMtype):
        Ylabels=['Pump on - Inlet closed','Pump off - Inlet open','Leak']
        if PMtype=='TEOM':
            headerLabels=['Main','Aux']

        if PMtype=='Dichot':
            headerLabels=['PM2.5','Coarse','Bypass']

        colNum=1
        rowNum=2
        count=0
        values={}


        for x in headerLabels:
            Label(frame, text=x,width=10).grid(row=1,column=colNum,sticky='news')
            colNum+=1

        for y in Ylabels:
            Label(frame, text=y,width=30).grid(row=rowNum,column=0,sticky='news')
            rowNum+=1

        for a in range(2,rowNum):               
            print(a)
            for b in range(1,colNum):

                print(b)
                values[count]=Entry(frame ,width=10).grid(row=a,column=b,sticky='news')
                count+=1

        print(count)
        print(values)


    ''' ---------------------------------------------------------------------------------------------------------------'''
    ''' ADD CALIBRATION SUBMISSION FORM '''
    ''' ---------------------------------------------------------------------------------------------------------------'''
    def addCal(self,frameMaster,count):

        backgroundClr='red'
        count22=0
        nowTime={}

        frame_Bdr=Frame(frameMaster,background='black')
        frame_Bdr.grid(row=0,column=0,sticky='ew',padx=10,pady=10)

        frame=Frame(frame_Bdr,background=backgroundClr)
        frame.grid(row=0,column=0,sticky='ew',padx=1,pady=1)

        '''INPUTS'''
        #CHANNEL
        Label(frame, text="Channel",width=20,background=backgroundClr).grid(row=2,column=0,sticky='news')
        cursor = self.db.cursor()
        cursor.execute("SELECT DISTINCT name FROM [EnvidasConfig].[dbo].[Channel]")
        res = cursor.fetchall() 
        channelName=StringVar(root)
        channelNum=StringVar(root)
        channelName.set(res[0])
        menu_channel = OptionMenu(frame,channelName,*res,command=lambda r=count22: channelNum.set(cursor.execute("""SELECT number FROM [EnvidasConfig].[dbo].[Channel] where name="""+channelName.get()+""" """)))
        menu_channel.grid(row=2, column=1,padx=0,pady=0,sticky='ew')
        menu_channel.config(background=backgroundClr)

        #STATUS
        Label(frame, text="Status",width=20,background=backgroundClr).grid(row=3,column=0,sticky='news')
        dataValid = StringVar(root)
        dataValid.set("Invalid")
        menu_validORnot=OptionMenu(frame,dataValid,'Invalid','Valid')
        menu_validORnot.grid(row=3, column=1,padx=0, pady=0,sticky='ew')
        menu_validORnot.config(background=backgroundClr)

        # CALIBRATION TIME
        Label(frame, text="Time",width=20,background=backgroundClr).grid(row=4,column=0,sticky='news')
        time = Entry(frame,width=20)
        time.grid(row=4, column=2,padx=2, pady=2,sticky='ew')
        nowTime[count22]=time
        timeIs=StringVar(root)
        timeIs.set('')
        B_time = Button(frame, text='Now',width=30,background=backgroundClr,command = lambda i=count22: self.insertTime(nowTime[i],'dt/'))
        B_time.grid(row=4,column=1,sticky='news')




        #ZERO REF
        Label(frame, text="Zero Ref",background=backgroundClr,width=10).grid(row=2,column=3,sticky='news')
        E_zeroRef = Entry(frame,width=10)
        E_zeroRef.grid(row=2,column=4,sticky='news')

        #ZERO MEAS
        Label(frame, text="Zero Meas",background=backgroundClr,width=10).grid(row=3,column=3,sticky='news')
        E_zeroMeas = Entry(frame ,width=10)
        E_zeroMeas.grid(row=3,column=4,sticky='news')

        #SPAN REF
        Label(frame, text="Span Ref",background=backgroundClr,width=10).grid(row=2,column=5,sticky='news')
        E_spanRef = Entry(frame ,width=10)
        E_spanRef.grid(row=2,column=6,sticky='news')

        #SPAN MEAS
        Label(frame, text="Span Meas",background=backgroundClr,width=10).grid(row=3,column=5,sticky='news')
        E_spanMeas = Entry(frame ,width=10)
        E_spanMeas.grid(row=3,column=6,sticky='news')


        '''CALCULATIONS'''
        #ZERO
        Label(frame, text="Zero",background=backgroundClr,width=7).grid(row=5,column=2,sticky='news')
        zeroVal = StringVar()
        Label(frame ,textvariable=zeroVal,background=backgroundClr, width=7).grid(row=5,column=3,sticky='news')

        #SPAN FACTOR
        Label(frame, text="Factor",background=backgroundClr,width=7).grid(row=5,column=4,sticky='news')
        factor = StringVar()
        Label(frame ,textvariable=factor,background=backgroundClr,width=7).grid(row=5,column=5,sticky='news')

        #SPAN DIFF
        Label(frame, text="Span Diff",background=backgroundClr,width=7).grid(row=5,column=6,sticky='news')
        spanDiff = StringVar()
        Label(frame ,textvariable=spanDiff,background=backgroundClr,width=7).grid(row=5,column=7,sticky='news')

        #CALC BUTTON
        B_calcCalib = Button(frame, text='Calculate',background=backgroundClr,width=30,command= lambda i=count22: ( 
            zeroVal.set( float(E_zeroMeas.get())-float(E_zeroRef.get()) ), 
            spanDiff.set(       (round(100*(100*float(E_spanMeas.get())/float(E_spanRef.get()))-100 ) )/100),
            factor.set(    (round(100*(float(E_spanRef.get())-float(E_zeroRef.get())) / (float(E_spanMeas.get())-float(E_zeroMeas.get()))))/100 )           
            ))
        B_calcCalib.grid(row=5,column=1)



        # SUBMIT BUTTON    
        B_submitCalib = Button(frame, text='Save',background=backgroundClr,width=30,command= lambda i=count22: submitCalibration(time.get(),channelName.get(),dataValid.get(),factor.get(),zeroVal.get(),E_zeroRef.get(),E_spanRef.get(),E_zeroMeas.get(),E_spanMeas.get())).grid(row=6,column=1)

        count22+=1


        def submitCalibration(t,c,v,f,zV,zR,sR,zM,sM):

            v='0'
            if v=='Valid':
                v='1'

            print(c)
            techA=str(c)
            techB=techA.replace(',','')
            techC=techB.replace('(','')
            techD=techC.replace(')','')
            techE=techD.replace(' ','')

            print(techE)

            sql_CNL="""
                SELECT number FROM [EnvidasConfig].[dbo].[Channel] WHERE name = '"""+techE+""""
                """
            cursor.execute(sql_CNL)
            cNum = int(cursor.fetchall())

            print(cNum)


            sql_CAL=""" 
                INSERT INTO [EnvidasConfig].[dbo].[DataCalibration] (Date_Time,channel,valid,factor,zero,zeroOffset,spanOffset,zeroDuration,spanDuration,zeroSamples,spanSamples,zeroRef,spanRef,zeroVal,spanVal,zeroSTD,spanSTD,multiPoint,calibSessionNumber)
                VALUES('"""+t+"""','"""+cNum+"""','"""+v+"""','"""+f+"""','"""+zV+"""','0','0','0','0','0','0','"""+zR+"""','"""+sR+"""','"""+zM+"""','"""+sM+"""','-9999','-9999','0','-1')
                """
            print(sql_CAL)
            cursor.execute(sql_CAL)
            self.db.commit()



    def validEntry(self,a,b,lim_low,lim_upp,testCond):
        # PURPOSE - tests calibration entries to see if they are numbers, and if they are within the bounds set for that varialble. Changes font and background colour depending on test result

        # extract entry
        t=self.calibrations[b]
        g=t[a]

        test=0
        # test that the entry contains anything
        if g.get():
            try: # test to see if number
                number=float(g.get())
                test=1
            except:
                g.configure(background='red')

        if test==1: # value is a number test  for validity
            if testCond == 'between':                
                if number < float(lim_low) or number > float(lim_upp):
                    g.configure(fg='white',background='black')
                    test=2

            if testCond == 'equal':
                if number != float(lim_low):
                    g.configure(fg='white',background='black')
                    test=2

            if testCond =='or': 
                if (number != float(lim_low)) and (number !=float(lim_upp)):
                    g.configure(fg='white',background='black')
                    test=2

            if test!=2: # if value is a number and is valid
                    g.configure(fg='black',background='white')

        # needs to return true or it will cancel the widget : http://stackoverflow.com/questions/24268503/how-do-i-make-tkinter-entry-focusout-validation-happen-more-than-just-the-first
        return('true')



 
    '''================================================================================================================'''
    ''' SAVE ENTRIES TO DATABASE '''
    '''================================================================================================================'''
        
    def saveEntries(self):
        # PURPOSE - 

        print(' ')
        print('>>>>>>>>>>>>>>>>>>> SAVING INFORMATION')

        #-----------------------------------------------------------------------
        # SAVE DETAILS FROM MAIN FORM
        #-----------------------------------------------------------------------

        # Save each entry one at a time
        for number, (self.stnID,self.tech,self.type,self.stTime,self.edTime,self.dataV,self.cat,self.serialN,self.issue,self.desc,self.spare,self.future) in enumerate(self.all_entries):

            # Construct entry
            self.Entry= '**' +self.cat.get()+ '**' +self.serialN.get()+ '**' +self.issue.get("1.0",END)+ '**' +self.desc.get("1.0",END)+ '**' +self.spare.get("1.0",END)+ '**' +self.future.get("1.0",END) + '**' + self.edTime.get()

            techA=str(self.tech.get())
            techB=techA.replace(',','')
            techC=techB.replace('(','')
            techD=techC.replace(')','')
            techE=techD.replace(' ','')
            techF=techE.replace('\'','')

            stnIDA=str(self.stnID.get())
            stnIDB=stnIDA.replace(',','')
            stnIDC=stnIDB.replace('(','')
            stnIDD=stnIDC.replace(')','')
            stnIDE=stnIDD.replace(' ','')
            stnIDF=stnIDE.replace('\'','')

            # Convert data valid response to integer
            isValid='0'
            if self.dataV.get()=='Yes':
                isValid='1'
                
            # Construct SQL statement
            sql_ADD=""" 
                INSERT INTO [EnvidasConfig].[dbo].[TB_LOGBOOK] (Date_Time,StationID,Equipment,TendType,InvalidData,Technician,Description,SpareParts)
                VALUES ('"""+self.stTime.get()+"""','"""+stnIDF+"""','Soil','"""+self.type.get()+"""','"""+isValid+"""','"""+techF+"""','"""+self.Entry+"""','')
            """
            print(sql_ADD)

            # Save Entry to database if the category is not 'Ignore' and the entry date is unique (if not unique then prompts user for action - replace or skip)
            if self.cat.get()!='Ignore':
                self.repeatAction='Save'
                cursor = self.db.cursor()

                print('Testing if date unique')
                print(self.stTime.get())
                sql_testTime="""
                    SELECT * FROM [EnvidasConfig].[dbo].[TB_LOGBOOK] WHERE Date_Time= '""" + self.stTime.get() +"""'
                    """
                timeTestresults=cursor.execute(sql_testTime)
                res=timeTestresults.fetchall()
                e=[]
                for i in res:
                    e.append(i)

                # If duplicate entry listed in database prompt user to replace or skip saving the new entry
                if len(e)>0:
                    print('Repeat database entry found')
                    self.repeatAction='Save'
                    self.repeatEntry_window(self.cat.get(),self.serialN.get())

                # If replace, then delete the old entry
                if self.repeatAction == 'Replace':
                    print('Removing original database entry')
                    sql_REMOVE="""
                        DELETE FROM [EnvidasConfig].[dbo].[TB_LOGBOOK] WHERE Date_Time= '""" + self.stTime.get()+"""' 
                        """
                    cursor.execute(sql_REMOVE)
                    self.db.commit()  
                    self.repeatAction='Save'

                # if no duplicate, or old entry was deleted then save new entry
                if self.repeatAction == 'Save':
                    print('Saving new database entry: '+self.cat.get()+' '+self.serialN.get())
                    cursor.execute(sql_ADD)
                    self.db.commit()

                # if user asked to skip
                else:
                    print('Not saving new database entry')


            #---------------------------------------------------------------
            # SAVE CALIBRATION/MAINTENANCE FORM IF THERE IS ONE
            #---------------------------------------------------------------
            # Try to find calibration 
            ifEntry=0 
            try:
                a=self.calibrations[number]
                print('Calibration found')

                # construct calibration entry
                instType = self.instType[number]
                instNum =  self.instSerial[number]
                formRev = self.revision[number]
                calibEntry=instType +'**' + formRev +'**'+ instNum + '**'
                for x in range(0,len(a),1):
                    entry=str(a[x].get())
                    calibEntry+=entry
                    if x < len(a)-1:
                        calibEntry +='**'
                ifEntry=1
                print(calibEntry)

            # If no calibration found
            except:
                print('No calibration found')

            # Save maintenance/calibration entry to database unless category is 'ignore' or no entry found
            if (self.cat.get()!='Ignore' and ifEntry==1):
                self.repeatAction='Save'
                cursor = self.db.cursor()
                time=self.stTime.get() + ':01'

                # make sure start time is specified
                if time != ':01':

                    print('Testing if date unique')
                    sql_testTime="""
                       SELECT * FROM [EnvidasConfig].[dbo].[TB_LOGBOOK] WHERE Date_Time= '""" + time +"""'
                       """
                    timeTestresults=cursor.execute(sql_testTime)
                    res=timeTestresults.fetchall()
                    e=[]
                    for i in res:
                        e.append(i)

                    # If duplicate entry listed remove old entry
                    if (len(e)>0 and self.repeatAction =='Save'):

                        print('Removing original calibration database entry')
                        sql_REMOVE="""
                           DELETE FROM [EnvidasConfig].[dbo].[TB_LOGBOOK] WHERE Date_Time= '""" + time +"""' 
                            """
                        cursor.execute(sql_REMOVE)
                        self.db.commit()

                    # Save entry
                    if self.repeatAction =='Save':    

                        sql_ADD=""" 
                            INSERT INTO [EnvidasConfig].[dbo].[TB_LOGBOOK] (Date_Time,StationID,Equipment,TendType,InvalidData,Technician,Description,SpareParts)
                            VALUES ('"""+time+"""','"""+stnIDF+"""','TRS','"""+self.type.get()+"""','"""+isValid+"""','"""+techF+"""','"""+calibEntry+"""','')
                        """
                        print(sql_ADD)     

                        print('Submitting calibration to database')                 
                        cursor.execute(sql_ADD)
                        self.db.commit()

                # If no start time specified
                else:
                    print('No start time specified')


    ''' USER SELECTS ACTION FOR A REPEAT DATABASE ENTRY '''
    ''' -------------------------------------------'''

    def andCloseWindow(self,window):
        # PURPOSE - close window that is passed to it
        window.destroy()

    def assignValue(self,var):
        # PURPOSE - used by create_window - assigns selection to self.repeatAction
        self.repeatAction=var        

    def repeatEntry_window(self,a,b):  
        # PURPOSE - used by saveEntries if it finds repeat database entry based  on duplicate time stamp
        # -> opens new window and prompts the user to select whether to replace or skip putting entry into database  

        # create new window
        repeatWindow = Toplevel()
        repeatWindow.grab_set() # makes window only one accessible

        # Tells user which entry is a repeat
        Label(repeatWindow,text='Repeat time stamp for database entry found: '+a+' '+ b).grid(row=0,column=0,columnspan=2, sticky='news',padx=5,pady=5)

        # Action Buttons - replace or skip putting entry into database
        Button(repeatWindow,text='Replace',width=20,command=lambda:(self.assignValue('Replace'),self.andCloseWindow(repeatWindow))).grid(row=2,column=0,sticky='news',padx=5,pady=5)
        Button(repeatWindow,text='Skip',width=20,command=lambda: (self.assignValue('Skip'),self.andCloseWindow(repeatWindow))).grid(row=2,column=1,sticky='news',padx=5,pady=5)

        repeatWindow.wait_window() # waits for user selection





''' MAIN LOOP'''
'''-------------------------------------------''' 
root = Tk()
MainWindow(root)
root.mainloop()
